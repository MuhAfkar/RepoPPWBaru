from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Manager
from unittest.mock import patch
from .views import index, friend_list, get_friend_list, add_friend, delete_friend, validate_npm, model_to_dict
from .api_csui_helper.csui_helper import CSUIhelper
from .models import Friend

# Create your tests here.
class lab7UnitTest(TestCase):
    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_get_friend_list_data_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_auth_param_dict(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])

    def test_add_friend(self):
        response_post = Client().post(
            '/lab-7/add-friend/',
            {'name': "idcp", 'npm': "1606", 'alamat': " ", 'ttl': " ", 'prodi': " "}
        )
        self.assertEqual(response_post.status_code, 200)

    def test_validate_npm(self):
        response = self.client.post('/lab-7/validate-npm/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(html_response, {'is_taken': False})

    def test_delete_friend(self):
        friend = Friend.objects.create(friend_name="Bruno Mars", npm="1601231234")
        response = Client().post('/lab-7/friend-list/delete-friend/' + str(friend.id) + '/')
        self.assertEqual(response.status_code, 302)
        self.assertNotIn(friend, Friend.objects.all())

    def test_invalid_sso(self):
        csui_helper = CSUIhelper()
        with self.assertRaises(Exception) as context:
            csui_helper.instance.get_access_token("ganteng", "jelas")
        self.assertIn("ganteng", str(context.exception))

    def test_paginator(self):
        data = [1,2,3,4,5,6]
        paginator = Paginator(data,3)
        page1 = paginator.page(1)
        page2 = paginator.page(2)
        self.assertEqual(page1.object_list, list(data[0:3]))
        self.assertEqual(page2.object_list, list(data[3:6]))
