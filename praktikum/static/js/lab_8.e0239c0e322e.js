// FB initiation function
window.fbAsyncInit = () => {
    FB.init({
      appId      : '146704592630000',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
    FB.getLoginStatus(function (response) {
      if (response.status === 'connected') {
          render(true);
      }
      else if (response.status === 'not_authorized') {
          render(false);
      }
      else {
          render(false);
      }
    });
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.

      $('.navbar-nav').html(
        '<li><button class = "btn-default logout-nav" onclick="facebookLogout()" style="border: rgb(249,249,249); background-color: rgb(249,249,249);transform: translateY(12px); color: grey;">Logout</button></li>'
      )

      getUserData(user => {
        console.log(user);
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
          '<div class="profile">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' + '<br>' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<p id = "nama">' + user.name + '</p>' +
            '<div class="data col-sm-4">' +
              '<p id = "aboutMe">About Me</p>' +
              '<p class = "keyData">Status</p>' +
              '<p class = "valueData">' + user.about + '</p>' +
              '<p class = "keyData">Email</p>' +
              '<p class = "valueData">' + user.email + '</p>' +
              '<p class = "keyData">Gender</p>' +
              '<p class = "valueData">' + user.gender + '</p>' +
            '</div>' +
          '</div>' +
          // '<div class = "col-sm-1"> </div>' +
          '<div class = "fbAddFeed rightColumn col-sm-7">' +
            '<p id = "tambah">Post New Status</p>' +
            '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" /> <br>' +
            '<button class="postStatus btn btn-default btn-primary" onclick="postStatus()">Post ke Facebook</button>' +
          '</div>'
        );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            var username = user.name;
            var profpic = user.picture.data.url;
            var userID = value.id.split("_")[0];
            var messageId = value.id.split("_")[1];
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="col-sm-4"></div>' +
                  '<div class="flex feed rightColumn col-sm-7">' +
                  '<div class="flex" style="width:100%; display:flex;">' +
                  
                    '<div class="flex-item" style="margin-top:10px">' +
                    '<img src="' + profpic + '" alt="profile picture" align="center" width = "40px" height = "40px" style="border-radius:50%">' +
                    '</div>' +
                    '<div class="flex-item" style="width: 87%;padding-left:15px;margin-top:10px">' +
                      '<b>' + username + '</b>' +
                      '<div class = "flex-item-status" style="font-size: 15px;word-wrap:break-word;">' + 
                        '<p>' + value.message + '</p>' +
                        '<p>' + value.story + '</p>' +          
                        '<div id="delete-button">' +
                        '<button class="btn btn-danger" onclick="deletePost('+ userID + "," + messageId + ')">delete</button>' +
                        '</div>' +
                      '</div>' +
                    '</div>' +
                  '</div>' +
                  '</div>' +
                '</div>'
              );
            } else if (value.message) {
              $('#lab8').append(
                '<div class="col-sm-4"></div>' +
                '<div class="flex feed rightColumn col-sm-7">' +
                '<div class="flex" style="width:100%; display:flex;">' +
                
                  '<div class="flex-item" style="margin-top:10px">' +
                      '<img src="' + profpic + '" alt="profile picture" align="center" width = "40px" height = "40px" style="border-radius:50%">' +
                  '</div>' +
                  '<div class="flex-item" style="width: 87%;padding-left:15px;margin-top:10px">' +
                    '<b>' + username + '</b>' +
                    '<div class = "flex-item-status" style="font-size: 15px;word-wrap:break-word;">' + 
                      '<p>' + value.message + '</p>' +
                    '</div>' +
                    '<div id="delete-button">' +
                    '<button class="btn btn-danger" onclick="deletePost('+ userID + "," + messageId + ')">delete</button>' +
                    '</div>' +
                  '</div>' +
                '</div>' +
                '</div>' +
                '</div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="col-sm-4"></div>' +
                '<div class="feed rightColumn col-sm-7">' +
                '<div class="flex" style="width:100%; display:flex;">' +
                  '<div class="flex-item" style="margin-top:10px">' +
                  '<img src="' + profpic + '" alt="profile picture" align="center" width = "40px" height = "40px" style="border-radius:50%">' +
                  '</div>' +
                  '<div class="flex-item" style="width: 87%;padding-left:15px;margin-top:10px">' +
                    '<b>' + username + '</b>' +
                    '<div class = "flex-item-status" style="font-size: 15px;word-wrap:break-word;">' + 
                      '<p>' + value.story + '</p>' +              
                    '</div>' +
                    '<div class="delete-button">' +
                    '<button class="btn btn-default" onclick="deletePost('+ userID + "," + messageId + ')">delete</button>' +
                    '</div>' +
                  '</div>' +
                '</div>' +
                '</div>' +
                '</div>'
              );
            }
          });
        });
      });


    } else {
      // Tampilan ketika belum login
      $('#lab8').html('<br><button class="loginBtn loginBtn--facebook" onclick="facebookLogin()">Login with Facebook</button>');
      $('.status').html('');
    }
  };

  const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    FB.login(function (response) {
      console.log(response);
      render(true);
    },
    {
      scope: 'public_profile,user_posts,publish_actions,user_about_me,email'
    })
  };

  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    FB.getLoginStatus(function (response) {
      if (response.status === 'connected') {
        FB.logout(function() {
          location.reload();
        });
      }
    });
  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  const getUserData = (fun) => {
    FB.getLoginStatus(function (response) {
      if (response.status === 'connected') {
          FB.api('/me?fields=id,name,about,email,gender,cover,picture.width(168).height(168)', 'GET', function (response) {
              console.log(response);
              if (response && !response.error) {
                  /* handle the result */
                  picture = response.picture.data.url;
                  name = response.name;
                  fun(response);
              }
              else {
                  alert("Something went wrong");
              }
          });
      }
    });
  };

  const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    FB.getLoginStatus(function (response) {
      if (response.status === 'connected') {
          FB.api('/me/posts', 'GET', function (response) {
              console.log(response);
              if (response && !response.error) {
                  /* handle the result */
                  fun(response);
              }
              else {
                  alert("Something went wrong");
              }
          });
      }
    });
  };

  const postFeed = (message) => {
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
    FB.api('/me/feed', 'POST', {message: message});
    render(true);
    location.reload();
  };

  const postStatus = () => {
    const message = $('#postInput').val();
    $('#postInput').val("");
    postFeed(message);
  };

  const deletePost = (user,message) => {
    FB.api("/"+user+"_"+message,"delete", function (response){
        if (response && !response.error){
            window.location.reload();
        }
        else{
            window.alert("error deleting message");
            console.log(response);
        }
    });
  };


