localStorage.setItem("themes", JSON.stringify([
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]));

localStorage.setItem("selectedTheme", JSON.stringify(
    {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}
));
$('body').css({"backgroundColor": JSON.parse(localStorage.selectedTheme).Indigo.bcgColor});
$('.text-center').css({"color": JSON.parse(localStorage.selectedTheme).Indigo.fontColor});

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  // ketika ingin menghapus
  if (x === 'ac') {
    print.value = '0'; // value dirubah jadi 0
    erase = false; // value erase menjadi false agar masih bisa menambahkan nilai pada screen calculator
  } else if (x === 'eval') { // ketika memencet sama dengan
    print.value = Math.round(evil(print.value) * 10000) / 10000; // mengeluarkan nilai
    erase = true; // value erase menjadi true agar nilai bisa langsung berubah ketika ingin membuat persamaan
  } else if (x === 'sin') {
    print.value = Math.round(evil(Math.sin(print.value) * 10000)) / 10000; // mengeluarkan nilai
    erase = true; // value erase menjadi true agar nilai bisa langsung berubah ketika ingin membuat persamaan
  } else if (x === 'tan') { 
    print.value = Math.round(evil(Math.tan(print.value) * 10000)) / 10000; // mengeluarkan nilai
    erase = true; // value erase menjadi true agar nilai bisa langsung berubah ketika ingin membuat persamaan
  } else if (x === 'log') {
    print.value = Math.round(evil(Math.log(print.value) * 10000)) / 10000;
    erase = true; // value erase menjadi true agar nilai bisa langsung berubah ketika ingin membuat persamaan
  } else if (erase === true) { // ketika value erase adalah true
    print.value = x; // value print menjadi x
    erase = false // dan erase menjadi false
  } else {
      if (print.value === '0') { // ketika valuenya 0, yaitu ketika di hapus, maka value akan menjadi yg dimasukkan pada awalnya
        print.value = x;
      } else {
        print.value += x;
      }
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// calc end

$(document).ready(function() {

  $('.my-select').select2({
      // mendapatkan data data dari localstorage yg keynya adalah themes yg telah didefinisikan sebelumnya
      data: JSON.parse(localStorage.getItem('themes'))
  });
  $('.apply-button').on('click', function(){ 
    // menadpatkan value yg diklik
    theme = JSON.parse(localStorage.getItem('themes'))[$('.my-select').val()];
    // mengganti background dan font dengan warna yg diklik
    $('body').css({"backgroundColor": theme['bcgColor']});
    $('.text-center').css({"color": theme['fontColor']});
    // mengeset selectedtheme menjadi tema yg telah diklik
    localStorage.setItem('selectedTheme', JSON.stringify(theme));  
    });

    $('textarea').keypress(function(event) {
    // ketika user mengepress enter
    if (event.which == 13) {
      // mendapatkan value pada textarea
      var msg = $('textArea').val();
      // mendapatkan value html pada kelas msg.insert
      var old = $('.msg-insert').html();
      // ketika msg kosong, maka tidak bisa dimasukkan ke tempat memunculkan value
      if (msg === ''){
        alert("Input tidak boleh kosong!");
      }
      else{
        // menambahkan msg pada kelas msg-insert
        $('.msg-insert').html(old + '<p class="msg-send">' + msg + '</p>')
        // mengeset value text area agar menjadi seperti semula
        $('textarea').val("");
      }
      // menjadikan text area kembali ke default awal
      event.preventDefault();
    }
  });
  
});

$('#chat-down').click(function () {
  // kolom chat di toggle ke bawah agar valuenya hidden
  $('.chat-body').slideToggle();
});







