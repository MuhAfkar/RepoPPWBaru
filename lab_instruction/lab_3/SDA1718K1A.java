import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class SDA1718K1A {
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		int banyak = Integer.parseInt(in.readLine());
		
		int[] arr = new int[banyak];
		
		StringTokenizer st = new StringTokenizer(in.readLine());
		
		for(int i = 0; i < banyak; i++){
			
			arr[i] = Integer.parseInt(st.nextToken());
			
		}
		
		int res = 0;
		for(int i = 0; i < arr.length; i++){
			if (i == arr.length-1) 
				res++;
			else if (arr[i] < arr[i+1]) 
				continue;
			else if (arr[i] == arr[i+1]) {
				res+=2;
				i++;
			}
			else if (arr[i] > arr[i+1]) {
				res++;
				i++;
			}
		}
		System.out.println(res);
		
	}
}
